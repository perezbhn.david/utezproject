import React, { Component } from 'react'
import LoginScreen from './src/screens/LoginScreen'
import HomeScreen from './src/screens/HomeScreen'
import AppNavigation from './src/navigation/AppNavigation'
export default class App extends Component {
  render() {
    return (
      <AppNavigation />
    )
  }
}
