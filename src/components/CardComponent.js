import React, {Component} from 'react';
import {View, Image, Text} from 'react-native';

export default class CardComponent extends Component {
  render() {
    return (
      <View
        style={{
          marginTop: 10,
          height: 180,
          width: 150,
          backgroundColor: 'white',
          margin: 5
        }}>
        <View>
          <Image
            style={{width: 150, height: 150}}
            source={{uri: this.props.image}}
          />
        </View>
        <View>
        <Text style={{fontWeight: 'bold'}}>{this.props.tittle}</Text>
        </View>
      </View>
    );
  }
}
