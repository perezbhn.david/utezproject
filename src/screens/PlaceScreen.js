import React, {Component} from 'react';
import {Text, View, Image, SafeAreaView, AsyncStorage} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';

export class PlaceScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      placesItem: null,
      placesItem2: [
        {
          id: 1,
          image:
            'https://media-cdn.tripadvisor.com/media/photo-s/0d/dc/93/b6/hotel-aventura.jpg',
          tittle: 'Super anfitrion',
          country: 'Morelos',
          precio: 2499,
          guess: 2,
          start: 3.6,
          host: 66,
        },
        {
          id: 2,
          image:
            'https://media-cdn.tripadvisor.com/media/photo-s/11/0f/d7/20/super-recomendado-este.jpg',
          tittle: 'Super anfitrion',
          country: 'CDMC',
          precio: 100,
          guess: 3,
          start: 4.1,
          host: 123.0,
        },
        {
          id: 3,
          image:
            'https://www.tecnohotelnews.com/wp-content/uploads/2018/11/barcel%C3%B3-imagine-vanguardia.jpg',
          tittle: 'Super anfitrion',
          country: 'Puebla',
          precio: 300,
          guess: 4,
          start: 4.5,
          host: 133,
        },
        {
          id: 4,
          image:
            'https://thumbnails.trvl-media.com/3ZbDCM6jYOi2pepZ1gta0soiCXY=/582x388/smart/filters:quality(60)/images.trvl-media.com/hotels/21000000/20200000/20199100/20199029/3d3435fb_y.jpg',
          tittle: 'Super anfitrion',
          country: 'Morelos',
          precio: 4999,
          guess: 4,
          start: 2.4,
          host: 3,
        },
        {
          id: 5,
          image:
            'https://www.hoteleskinky.com/img/hotel/blog/love-hotel-le-reve-vanguardia-que-despierta-a-tus-sentidos/love-hotel-le-reve-vanguardia-que-despierta-a-tus-sentidos.jpg',
          tittle: 'Super anfitrion',
          country: 'Sinaloa',
          precio: 3000,
          guess: 4,
          start: 3.9,
          host: 72,
        },
      ],
    };
    this.getPlace();
  }
  getPlace = async () => {
    const listPlaces = await fetch('http://skychar.store/airbnb').then(r =>
      r.json(),
    );
    await AsyncStorage.setItem('@places', JSON.stringify(listPlaces));
    this.setState({
      placesItem: JSON.parse(await AsyncStorage.getItem('@places')),
    });
  };
  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#2196F3'}}>
        <View style={{alignItems: 'center', marginTop: 10}}>
          <Text style={{color: 'white', fontSize: 24}}>PLACE SCREEN</Text>
        </View>

        <FlatList
          extraData={this.state}
          data={this.state.placesItem}
          keyExtractor={item => item.id.toString()}
          renderItem={({item}) => (
            <View style={{backgroundColor: 'white', marginTop: 10}}>
              <View>
                <Image
                  style={{width: '100%', height: 150}}
                  source={{
                    uri: item.image,
                  }}
                />
              </View>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <View style={{flexDirection: 'row'}}>
                  {item.host >= 1 ? (
                    <View>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          borderRadius: 5,
                          borderWidth: 2,
                        }}>
                        {item.title}{' '}
                      </Text>
                    </View>
                  ) : null}
                  <View>
                    <Text>{item.guess}</Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <View>
                    <Text>{item.star}</Text>
                  </View>
                  <View>
                    <Text>({item.host})</Text>
                  </View>
                </View>
              </View>
              <View>
                <Text>{item.country}</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={{fontWeight: 'bold'}}>${item.price}</Text>
                <Text> Por noche</Text>
              </View>
            </View>
          )}
        />
      </SafeAreaView>
    );
  }
}

export default PlaceScreen;
