import React, {Component} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Image, Text, View} from 'react-native';
import {ScrollView, TouchableHighlight} from 'react-native-gesture-handler';

export default class DetailsScreen extends Component {
  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <ScrollView style={{flex: 0.2}}>
          <Image
            style={{width: '100%', height: 150}}
            source={{
              uri:
                'https://www.tecnohotelnews.com/wp-content/uploads/2018/11/barcel%C3%B3-imagine-vanguardia.jpg',
            }}
          />
          <View>
            <Text style={{fontSize: 20, color: 'red'}}>Libre</Text>
            <Text style={{fontSize: 40}}>CASA ENTERA</Text>
          </View>
          <View>
            <Text>Douron Villa with swimming pool</Text>
          </View>
          <View>
            <Text>Penafiel</Text>
          </View>
          <View>
            <Text>Casa entera</Text>
            <Text>9 huéspedes</Text>
          </View>
          <View>
            <Text>Traducir</Text>
            <Text>You are welcome</Text>
          </View>
          <View>
            <Text>Lugares para dormir</Text>
          </View>
          <View>
            <Text>Penafiel</Text>
          </View>
          <View>
            <Text>Casa entera</Text>
            <Text>9 huéspedes</Text>
          </View>
          <View>
            <Text>Traducir</Text>
            <Text>You are welcome</Text>
          </View>
          <View>
            <Text>Lugares para dormir</Text>
          </View>
          <View>
            <Text>Penafiel</Text>
          </View>
          <View>
            <Text>Casa entera</Text>
            <Text>9 huéspedes</Text>
          </View>
          <View>
            <Text>Traducir</Text>
            <Text>You are welcome</Text>
          </View>
          <View>
            <Text>Lugares para dormir</Text>
          </View>
          <View>
            <Text>Penafiel</Text>
          </View>
          <View>
            <Text>Casa entera</Text>
            <Text>9 huéspedes</Text>
          </View>
          <View>
            <Text>Traducir</Text>
            <Text>You are welcome</Text>
          </View>
          <View>
            <Text>Lugares para dormir</Text>
          </View>
          <View>
            <Text>Penafiel</Text>
          </View>
          <View>
            <Text>Casa entera</Text>
            <Text>9 huéspedes</Text>
          </View>
          <View>
            <Text>Traducir</Text>
            <Text>You are welcome</Text>
          </View>
          <View>
            <Text>Lugares para dormir</Text>
          </View>
          <View>
            <Text>Penafiel</Text>
          </View>
          <View>
            <Text>Casa entera</Text>
            <Text>9 huéspedes</Text>
          </View>
          <View>
            <Text>Traducir</Text>
            <Text>You are welcome</Text>
          </View>
          <View>
            <Text>Lugares para dormir</Text>
          </View>
          <View>
            <Text>Penafiel</Text>
          </View>
          <View>
            <Text>Casa entera</Text>
            <Text>9 huéspedes</Text>
          </View>
          <View>
            <Text>Traducir</Text>
            <Text>You are welcome</Text>
          </View>
          <View>
            <Text>Lugares para dormir</Text>
          </View>
          <View>
            <Text>Penafiel</Text>
          </View>
          <View>
            <Text>Casa entera</Text>
            <Text>9 huéspedes</Text>
          </View>
          <View>
            <Text>Traducir</Text>
            <Text>You are welcome</Text>
          </View>
          <View>
            <Text>Lugares para dormir</Text>
          </View>
          <View>
            <Text>Penafiel</Text>
          </View>
          <View>
            <Text>Casa entera</Text>
            <Text>9 huéspedes</Text>
          </View>
          <View>
            <Text>Traducir</Text>
            <Text>You are welcome</Text>
          </View>
          <View>
            <Text>Lugares para dormir</Text>
          </View>
          <View>
            <Text>Penafiel</Text>
          </View>
          <View>
            <Text>Casa entera</Text>
            <Text>9 huéspedes</Text>
          </View>
          <View>
            <Text>Traducir</Text>
            <Text>You are welcome</Text>
          </View>
          <View>
            <Text>Lugares para dormir</Text>
          </View>
        </ScrollView>

        <View style={{flex: 0.2, backgroundColor: 'white'}}>
          <View style={{flex: 0.2}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Text
                onPress={this.testingLogin}
                style={{fontSize: 24, color: 'black'}}>
                $Noche
              </Text>
              <TouchableHighlight
                style={{
                  borderRadius: 5,
                  borderColor: 'white',
                  borderWidth: 1,
                  backgroundColor: 'red',
                }}>
                <Text
                  onPress={this.testingLogin}
                  style={{fontSize: 24,
                    flexDirection: 'row',
                     color: 'white'}}>
                  Verificar disponibilidad
                </Text>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
