import React, {Component} from 'react';
import {SafeAreaView, View, Text, Image} from 'react-native';
import {ScrollView, TouchableHighlight} from 'react-native-gesture-handler';
import {FlatList} from 'react-native-gesture-handler';

export default class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      placesItem: [
        {
          id: 1,
          image:
            'https://media-cdn.tripadvisor.com/media/photo-s/0d/dc/93/b6/hotel-aventura.jpg',
          tittle: 'Departamento',
          country: 'Morelos',
          precio: 2499,
          start: 3.6,
          host: 66,
        },
        {
          id: 2,
          image:
            'https://media-cdn.tripadvisor.com/media/photo-s/11/0f/d7/20/super-recomendado-este.jpg',
          tittle: 'Departamento',
          country: 'CDMC',
          precio: 100,
          start: 4.1,
          host: 123.0,
        },
        {
          id: 3,
          image:
            'https://www.tecnohotelnews.com/wp-content/uploads/2018/11/barcel%C3%B3-imagine-vanguardia.jpg',
          tittle: 'Departamento',
          country: 'Puebla',
          precio: 300,
          start: 4.5,
          host: 133,
        },
        {
          id: 4,
          image:
            'https://thumbnails.trvl-media.com/3ZbDCM6jYOi2pepZ1gta0soiCXY=/582x388/smart/filters:quality(60)/images.trvl-media.com/hotels/21000000/20200000/20199100/20199029/3d3435fb_y.jpg',
          tittle: 'Departamento',
          country: 'Morelos',
          precio: 4999,
          start: 2.4,
          host: 3,
        },
        {
          id: 5,
          image:
            'https://www.hoteleskinky.com/img/hotel/blog/love-hotel-le-reve-vanguardia-que-despierta-a-tus-sentidos/love-hotel-le-reve-vanguardia-que-despierta-a-tus-sentidos.jpg',
          tittle: 'Departamento',
          country: 'Sinaloa',
          precio: 3000,
          start: 3.9,
          host: 72,
        },
      ],
    };
  }
  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <View style={{alignItems: 'center', marginTop: 10}}>
            <Text style={{color: 'black', fontSize: 24}}>
              Resataurante SCREEN
            </Text>
          </View>
          <View style={{margin: 5}}>
            <Text style={{fontSize: 20}}>Actividades cercanas</Text>
            <Text>
              Para este tipo de alojamiento, esta app te da al alcance de tu
              mano lo que siempre haz necesitado.
            </Text>
          </View>
          <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            extraData={this.state}
            data={this.state.placesItem}
            keyExtractor={item => item.id.toString()}
            renderItem={({item}) => (
              <TouchableHighlight
                onPress={() => this.props.navigation.navigate('Details')}>
                <View
                  style={{backgroundColor: 'white', margin: 10, width: 300}}>
                  <View>
                    <Image
                      style={{width: '100%', height: 150}}
                      source={{
                        uri: item.image,
                      }}
                    />
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      {item.host >= 1 ? (
                        <View>
                          <Text>{item.tittle} </Text>
                        </View>
                      ) : null}
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View>
                        <Text>{item.start}</Text>
                      </View>
                      <View>
                        <Text>({item.host})</Text>
                      </View>
                    </View>
                  </View>
                  <View>
                    <Text>{item.country}</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={{fontWeight: 'bold'}}>${item.precio}</Text>
                    <Text> Por noche</Text>
                  </View>
                </View>
              </TouchableHighlight>
            )}
          />
        </ScrollView>
      </SafeAreaView>
    );
  }
}
