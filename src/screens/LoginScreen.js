import React, {Component} from 'react';
import {SafeAreaView, View, Text, Image} from 'react-native';
import {
  TextInput,
  TouchableHighlight,
  ScrollView,
} from 'react-native-gesture-handler';

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: '',
      password: '',
    };
  }
  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#2196F3'}}>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <View style={{alignItems: 'center', marginTop: 10}}>
            <Text style={{color: 'white', fontSize: 24}}>REACT NATIVE</Text>
            <Text style={{color: 'white', fontSize: 24}}>
              {' '}
              {this.state.user}
            </Text>
            <Text style={{color: 'white', fontSize: 24}}>
              {' '}
              {this.state.password}
            </Text>
          </View>
          <View style={{alignItems: 'center'}}>
            <Image
              source={require('../../assets/img/logo2.png')}
              style={{height: 100, width: 100}}
            />
          </View>
          <View style={{alignItems: 'center', marginTop: 10}}>
            <Text style={{fontSize: 20, color: 'white'}}>
              Correo electrónico
            </Text>
            <TextInput
              onChangeText={text => this.setState({user: text})}
              value={this.state.user}
              style={{
                fontSize: 20,
                borderRadius: 5,
                marginTop: 5,
                backgroundColor: 'white',
                width: '90%',
              }}
              placeholder={'Correo electrónico'}
            />
          </View>
          <View style={{alignItems: 'center', marginTop: 10}}>
            <Text style={{fontSize: 20, color: 'white'}}>Contraseña</Text>
            <TextInput
              onChangeText={text => this.setState({password: text})}
              value={this.state.password}
              secureTextEntry={true}
              style={{
                fontSize: 20,
                borderRadius: 5,
                marginTop: 5,
                backgroundColor: 'white',
                width: '90%',
              }}
              placeholder={'Contraseña'}
            />
          </View>
          <View style={{alignItems: 'center', marginTop: 20}}>
            <TouchableHighlight
              style={{
                borderRadius: 5,
                borderColor: 'white',
                borderWidth: 1,
                paddingTop: 10,
                paddingBottom: 10,
                paddingLeft: 30,
                paddingRight: 30,
                backgroundColor: '#536DFE',
              }}>
              <Text
                onPress={this.testingLogin}
                style={{fontSize: 24, color: 'white'}}>
                Iniciar sesión
              </Text>
            </TouchableHighlight>
          </View>
          <View style={{alignItems: 'center', marginTop: 50}}>
            <Text style={{color: 'white'}}>
              Copyright Create Commons - ReactNative
            </Text>
            <Text style={{color: 'white'}}>Copied by David</Text>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
  testingLogin = () => {
    const data = {
      username: this.state.user,
      password: this.state.password,
    };
    this.props.navigation.navigate('BottomStack');
  };
}
