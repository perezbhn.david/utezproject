import React, {Component} from 'react';
import {SafeAreaView, View, Text, Image} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import CardComponent from '../components/CardComponent';

export default class HomeScreen extends Component {
  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#2196F3'}}>
        <ScrollView contentInsetAdjustmentBehavior="automatic">
          <View style={{alignItems: 'center', marginTop: 10}}>
            <Text style={{color: 'white', fontSize: 24, fontFamily: 'Monoton'}}>
              HOME SCREEN
            </Text>
          </View>
          <ScrollView showsHorizontalScrollIndicator={false}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              <CardComponent
                tittle={'Estancias'}
                image={
                  'https://comodecorarmicuarto.com/wp-content/uploads/2019/12/como-organizar-un-apartamento-peque%C3%B1o-moderno.jpg.webp'
                }
              />
              <CardComponent
                tittle={'Experiencias'}
                image={
                  'https://media-cdn.tripadvisor.com/media/photo-s/11/0f/d7/20/super-recomendado-este.jpg'
                }
              />
              <CardComponent
                tittle={'Aventura'}
                image={
                  'https://media-cdn.tripadvisor.com/media/photo-s/0d/dc/93/b6/hotel-aventura.jpg'
                }
              />
            </ScrollView>
          </ScrollView>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
